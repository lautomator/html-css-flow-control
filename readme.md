HTML Flow Control
=================

This is a reference for controlling the flow of block elements on
an HTML page.

* Download the src file: flow-control.html.
* Open in a text editor.
* Adjust the z-index property in the CSS.

